import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

if (Meteor.isServer) {
    Meteor.publish('usersList', () => {
        return Meteor.users.find({},{fields: {username: 1}});
    });
}
