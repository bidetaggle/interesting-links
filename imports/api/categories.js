import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Categories = new Mongo.Collection('categories');

if (Meteor.isServer) {
    Meteor.publish('categories', () => {
        return Categories.find();
    });
    Meteor.methods({
        'input.curl'(link){
            if(link.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)){
                try {
                    const result = HTTP.get(link,{
                        followRedirects: true,
                        headers: {
                            'Accept': 'text/html',
                            'Accept-Charset': 'utf-8',
                            'Content-Type': 'text/html; charset=utf-8',
                            //'User-Agent': 'Googlebot/2.1 (+http://www.google.com/bot.html)',
                            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0',
                            'HTTP2-Settings': 'token64',
                            'Upgrade': 'h2c, HTTPS/1.3, IRC/6.9, RTA/x11, websocket'
                            //'Content-Length': '348'
                        }
                    });
                    console.log(result.content);
                    let title = result.content.match(/<title.*>(.+)<\/title>/mi);
                    if(title == null){
                        title = result.content.match(/<\s*meta\s+property\s*=\s*"og:title"\s+content\s*=\s*"\s*(.+?)\s*"\s*>/mi);
                    }
                    return title[1];
                } catch (error) {
                    return error.message;
                    //throw new Meteor.Error(error.message);
                }
            }
            else
                console.log("not a valid website address");
        }
    });
}

Meteor.methods({
    'input.new-category'(name){
        check(name, String);

        //if not logged
        if(!Meteor.user()) throw new Meteor.Error('not-authorized');

        //insert
        Categories.insert({
            name,
            links: [],
            createdAt: new Date(),
            owner: Meteor.user().username
        });
    },
    'input.new-link'(url, description, categoryId){
        check(url, String);
        check(description, String);
        check(categoryId, String);

        //if not logged
        if(!Meteor.user())
            throw new Meteor.Error('not-authorized');
        //if categoryId doesn't exists (may be a hack attempt)
        if(!Categories.findOne({_id: categoryId}))
            throw new Meteor.Error(`category doesn't exist`);
        //if link already exists in the same category
        Categories.findOne({_id: categoryId}).links.map((link) => {
            if(link.url == url)
                throw new Meteor.Error(`This link has already been added to this category under the name of ${link.description}`);
        });

        //insert
        Categories.update({_id: categoryId},{
            $push: {
                links: {
                    _parentId: categoryId,
                    url: url,
                    description: description,
                    createdAt: new Date(),
                    owner: Meteor.user().username
                }
            }
        });
    },
    'category.remove'(id, owner){
        if(owner == Meteor.user().username)
            Categories.remove({_id: id});
    },
    'link.remove'(_parentId, url, owner){
        if(
            owner == Meteor.user().username
            || Categories.findOne({_id: _parentId, owner: Meteor.user().username})
        ){
            Categories.update(
                {_id: _parentId},
                { $pull: {
                    links: { url: url}
                }}
            );
        }
    }
});
