import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import './app-theme.html';

Template.appTheme.helpers({
    day(){
        return (Meteor._localStorage.getItem('app-theme') == 'day');
    },
    night(){
        return (Meteor._localStorage.getItem('app-theme') == 'night');
    },
    maraki(){
        return (Meteor._localStorage.getItem('app-theme') == 'maraki');
    },
});

Template.appTheme.events({
    'change select'() {
        let theme = $('#app-theme').val();
        Meteor._localStorage.setItem('app-theme', theme);
        $('#container').removeClass();
        $('#container').addClass(`theme-${theme}`);
    },
});
