import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Session } from 'meteor/session';
import Clipboard from 'clipboard';

import { Categories } from '../api/categories.js';
import './input.js';
import './app-theme.js';
import './index.html';

Accounts.ui.config({
    passwordSignupFields: 'USERNAME_ONLY'
});

Template.index.onCreated(() => {
    Meteor.subscribe('categories');
});

Template.index.helpers({
    username() {
        return FlowRouter.getParam('username') ? FlowRouter.getParam('username') : "bidetaggle";
    },
    is_connected() {
        return Meteor.status().connected;
    },
    displaySearchBar() {
        return !FlowRouter.getParam('category');
    },
    categories() {
        Session.setDefault('filter', '');

        let searchTemplate, orQuery;
        if(FlowRouter.getParam('category')){
            searchTemplate = FlowRouter.getParam('category');
            orQuery = [{name: searchTemplate}];
        }
        else{
            searchTemplate = {$regex: `.*${Session.get('filter')}.*`, $options: "i"};
            orQuery = [
                {name: searchTemplate},
                {"links.description": searchTemplate},
                {"links.url": searchTemplate}
            ];
        }

        let cats = Categories.find(
            { $and: [
                    {owner: Template.index.__helpers.get('username').call()},
                    { $or: orQuery }
                ]
            },
            {sort: {createdAt: -1}}
        );

        return cats.count() > 0 ? cats : false;
    }
});

Template.index.events({
    'keyup [name="search"]'(event){
        Session.set('filter', event.target.value);
        if(event.target.value == "")
            $('header').slideDown();
        else
            $('header').slideUp();
    },
    'focus [name="search"]'(event){
        $('.banner').slideUp();
    },
    'focusout [name="search"]'(event){
        $('.banner').slideDown();
    }
});

if (Meteor.isClient) {
    Template.index.onRendered(function() {
        var clipboard = new Clipboard('.btn-copy-link');
    });
}
