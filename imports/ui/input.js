import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Categories } from '../api/categories.js';
import { Session } from 'meteor/session';

import './category.js';
import './link.js';
import './input.html';
import { uiCallback } from './ui-callback';

Template.input.helpers({
    categoriesNumber() {
        return Categories.find({owner: FlowRouter.getParam('username')}).count();
    },
    categories() {
        return Categories.find({owner: FlowRouter.getParam('username')}, {sort: {createdAt: -1}});
    },
    isYourLinksList() {
        return Meteor.user() && Meteor.user().username == FlowRouter.getParam('username');
    },
    curl() {
        return Session.get('curl');
    },
    curl_awaiting() {
        return Session.get('curl_awaiting');
    }
});

Template.input.events({
    'submit .new-category'(event) {
        event.preventDefault();

        const target = event.target;
        const name = target.name.value;

        //input verification

        if(name == "")
            uiCallback(`You didn't type anything dude!`);
        else if(Categories.findOne({name: name}))
            uiCallback(`this category already exists`);
        //insert
        else{
            Meteor.call('input.new-category', name);

            //clear form
            target.name.value = '';

            // Close modale
            var modale = document.getElementById('showLink')
            modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
        }
    },
    'keyup [name="link"]'(event){
        if(event.target.value.match(/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)){
            Session.set('curl_awaiting', true);
            Meteor.call('input.curl', event.target.value, function(error, result){
                if(error)
                    uiCallback(error)
                else{
                    result = result.replace(/&quot;/g, '"').replace(/&amp;/g, '&');
                    Session.set('curl', result);
                    Session.set('curl_awaiting', false);
                }
            });
        }
    },
    'submit .new-link'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.target;

        const link = target.link.value;
        const description = target.description.value;
        const categoryId = target.category.value;

        let verification = true;
        //input verification
        if(description == "" || link == ""){
            verification = false;
            uiCallback(`All the fields have to be filled dude`);
        }
        let linksList = Categories.findOne({_id: categoryId}).links
        for(let i=0 ; i<linksList.length; i++)
            if(linksList[i].url == link){
                verification = false;
                uiCallback(`${linksList[i].owner} already added this link in this category under the name of "${linksList[i].description}"`);
                break;
            }

        //insert
        if(verification){
            Meteor.call('input.new-link', link, description, categoryId);

            // Clear form
            target.link.value = '';
            target.description.value = '';

            // Close modale
            var modale = document.getElementById('showLink')
            modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
        }
    },
});
