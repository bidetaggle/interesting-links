import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Categories } from '../api/categories.js';
import { uiCallback } from './ui-callback';
import './link.html';

Template.link.helpers({
    is_owner() {
        if(Meteor.user())
            return (
                this.owner === Meteor.user().username
                ||
                Categories.findOne({_id: this._parentId, owner: Meteor.user().username})
            );
        else {
            return false;
        }
    }
});

Template.link.events({
    'click .btn-copy-link'() {
        uiCallback("Copied in clipboard!");
    },
    'click .delete'() {
        Meteor.call('link.remove', this._parentId, this.url, this.owner);
    },
});
