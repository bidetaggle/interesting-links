import './routes.js';

if(!Meteor._localStorage.getItem('app-theme'))
    Meteor._localStorage.setItem('app-theme', 'night');

Platform = {
    isIOS: function () {
        return (!!navigator.userAgent.match(/iPad/i) || !!navigator.userAgent.match(/iPhone/i) || !!navigator.userAgent.match(/iPod/i)) || Session.get('platformOverride') === 'iOS';
    },
    isAndroid: function () {
        return Meteor.isCordova && navigator.userAgent.indexOf('Android') > 0 || Session.get('platformOverride') === 'Android';
    },
    isWebBrowser: function () {
        return !Meteor.isCordova;
    }
};
