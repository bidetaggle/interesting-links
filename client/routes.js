import '../imports/ui/app.js';

FlowRouter.route('/', {
    name: 'index',
    action(params, queryParams) {
        //FlowRouter.go('index.user', {username: 'bidetaggle'});
        BlazeLayout.render('app', {main: 'index'});
    }
});

FlowRouter.route('/@:username', {
    name: 'index.user',
    action(params, queryParams) {
        BlazeLayout.render('app', {main: 'index'});
    }
});

FlowRouter.route('/@:username/:category', {
    name: 'index.user.category',
    action(params, queryParams) {
        BlazeLayout.render('app', {main: 'index'});
    }
});

FlowRouter.route('/users-list', {
    name: 'users-list',
    action(params, queryParams) {
        BlazeLayout.render('app', {main: 'usersList'});
    }
});
