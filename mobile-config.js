App.info({
    id: 'com.interesting_links.release',
    name: 'Interesting links',
    version: "0.0.7"
});

App.icons({
    'android_mdpi': 'icons/thelema-48x48.png',
    'android_hdpi': 'icons/thelema-72x72.png',
    'android_xhdpi': 'icons/thelema-96x96.png',
    'android_xxhdpi': 'icons/thelema-144x144.png',
    'android_xxxhdpi': 'icons/thelema-192x192.png'
});

App.launchScreens({
    'android_mdpi_portrait': 'icons/launchscreen-320x480.png',
    'android_hdpi_portrait': 'icons/launchscreen-480x800.png',
    'android_xhdpi_portrait': 'icons/launchscreen-720x1280.png',
    'android_xxhdpi_portrait': 'icons/launchscreen-960x1600.png',
    'android_xxxhdpi_portrait': 'icons/launchscreen-1280x1920.png'
});

App.setPreference('BackgroundColor', '0xff272931');
App.setPreference('HideKeyboardFormAccessoryBar', false);

App.accessRule('http://*', {type: 'navigation'});
App.accessRule('https://*', {type: 'navigation'});
